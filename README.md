# README #

Коротенькое пояснение what the fuck is going on.

### Что это за репозиторий? ###

В данном репозитории располагаются исходные коды расширения Point#, 
которое предназначено для расширения функциональности сайта [Point.im](https://point.im/) и является идейным 
ответвлением расширения [Point+](https://bitbucket.org/skobkin/chrome_point_plus).

### Как заставить его работать? ###

Есть несколько вариантов. Выбирайте на свой вкус:

* Установить расширение для Google Chrome из [Google Web Store](https://chrome.google.com/webstore/detail/point/jebkkkanbjkflnhgnleeecjekccclnak)
* Установить расширение для Mozilla Firefox из [Mozilla Addons](https://addons.mozilla.org/ru/firefox/addon/point-sharp/)
* Установить расширение из [раздела Downloads на Bitbucket](https://bitbucket.org/NokitaKaze/chrome_point_plus-nokita-version/downloads)

### Как настраивать? ###

В адресной строке (омнибокс) появится иконка Point.im. Если на нёё нажать - появится окошко настроек.

![Настройки расширения](https://storage4.static.itmages.ru/i/15/0107/h_1420652338_6632200_307d80b672.png "Окно настроек расширения")

### Мне не хватает функций/я нашёл ошибку ###

* Милости прошу к [нашему шалашу](https://bitbucket.org/NokitaKaze/chrome_point_plus-nokita-version/issues?status=new&status=open)
* Присылать пулл-реквесты с язвительными замечаниями
* Поставить в магазинах приложений оценку